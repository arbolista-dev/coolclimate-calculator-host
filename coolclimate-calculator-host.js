(function(){



	var CALCULATOR_PATH;
	if (/^http:\/\/localhost\/coolclimate-calculator-host/.test(window.location.href)){
		CALCULATOR_PATH = {origin: "http://localhost", dir: "/coolclimate-calculators"}
	} else if (/^http:\/\/coolclimate.berkeley.edu\/coolclimate-calculator-host/.test(window.location.href)){
		CALCULATOR_PATH = {origin: "http://coolclimate.berkeley.edu", dir: "/calculators_staging"};
	} else {
		CALCULATOR_PATH = {origin: "http://coolclimate.berkeley.edu", dir: "/calculators"}
	}
	CALCULATOR_PATH.url = CALCULATOR_PATH.origin + CALCULATOR_PATH.dir + "/household/ui.php";

	window.CoolClimateCalculatorHost = function(container_id){
		var host = this,
			_iframe = document.createElement("IFRAME");
		_iframe.setAttribute("src", CALCULATOR_PATH.url);
		host._iframe = _iframe;

		window.addEventListener("message", function(e){ 
			if (e.origin === CALCULATOR_PATH.origin){
				if (e.data.type === "ready"){
					host.$ready();
					host.$stateChanged(e.data.state);
				} else if (e.data.type === "state-changed"){
					host.$stateChanged(e.data.change, e.data.state);
				} else if (e.data.type === "page-loaded"){
					host.$pageLoaded(e.data.page);
				}
			}
		}, false);

		document.getElementById(container_id).appendChild(_iframe);

		host.events = {
			"state-changed": [],
			"page-loaded": []
		};

		host.onReady = [];
		host.is_ready = false;
	};

	/*
	 * Events Called by Client (iFrame Parent)
	 */

	CoolClimateCalculatorHost.prototype.ready = function(fn){
		var host = this;
		if (host.is_ready){
			fn();
		} else {
			host.onReady.push(fn);
		}
	};
 
	CoolClimateCalculatorHost.prototype.setInputs = function(inputs, fn){
		var host = this;
		host.inputs = inputs;
		host._iframe.contentWindow.postMessage({type: "set-values", inputs: inputs}, CALCULATOR_PATH.origin);
	};

	CoolClimateCalculatorHost.prototype.on = function(event, fn){
		var host = this;

		if (host.events[event].constructor === Array){
			host.events[event].push(fn);
		};
	};	

	/*
	 *  Events called by Cool Climate Calculator
	 */

	 CoolClimateCalculatorHost.prototype.$ready = function(){
	 	var host = this;
	 	host._iframe.contentWindow.postMessage({type: 'set-host-origin', origin: window.location.protocol+ "//" + window.location.host}, CALCULATOR_PATH.origin)
	 	host.onReady.forEach(function(fn){
	 		fn();
	 	});
	 	host.is_ready = true;
	 };

	 CoolClimateCalculatorHost.prototype.$pageLoaded = function(page){
	 	var host = this;
	 	host.events["page-loaded"].forEach(function(fn){
	 		fn(page);
	 	});
	 };

	CoolClimateCalculatorHost.prototype.$stateChanged = function(change, state){
		var host = this;
		host.$parseState(state);
		host.events["state-changed"].forEach(function(fn){
			fn(change);
		});
	};

	CoolClimateCalculatorHost.prototype.$parseState = function(state){
		var host = this;
		host.inputs = {};
		host.results = {};
		for (var key in state){
			if (/^input_/.test(key)){
				host.inputs[key] = state[key];
			} else if (/^result_/.test(key)){
				host.results[key] = state[key];
			}
		}
	};

})();