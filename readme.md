# Cool Climate Calculator Host

This Javascript library enables you to embed the Cool Climate household calculator on a website and access the users inputs and results.

## Initialization

```javascript
	var calculator_container_id = "container_id";
	window.calculator_host = new CoolClimateCalculatorHost(calculator_container_id);
	calculator_host.ready(function(){
		// do stuff
	});
```

## Access Inputs and Results

At any point after the CoolClimateCalculatorHost#ready event, you can accss the CoolClimateCalculatorHost#inputs (API keys starting with "input_") and CoolClimateCalculatorHost#results (API keys starting with "result_").

```javascript
	var calculator_container_id = "container_id";
	window.calculator_host = new CoolClimateCalculatorHost(calculator_container_id);
	calculator_host.ready(function(){
		console.log(calculator_host.inputs);
		console.log(calculator_host.results);
	});
```

## Changing the User's Inputs

After the CoolClimateCalculatorHost#ready event, you can pass in an object of API keys with different answers to CoolClimateCalculatorHost#setInputs in order to prepopulate the user's calculator.

The `"state-changed"` event will fire once the inputs have changed and the results have been retrieved.

```javascript
	var calculator_container_id = "container_id";
	window.calculator_host = new CoolClimateCalculatorHost(calculator_container_id);
	calculator_host.ready(function(){
		var inputs = {input_footprint_transportation_miles1: 5000, input_location: "CA", input_location_mode: 4};
		calculator_host.setInputs(inputs);
	});
```

## Detect User Interaction

You can detect whenever a user changes an input by setting a listener on the `"state-changed"` event. Such as the following:

```javascript
	var calculator_container_id = "container_id";
	window.calculator_host = new CoolClimateCalculatorHost(calculator_container_id);
	calculator_host.ready(function(){
		calculator_host.on("state-changed", function(change){
			alert("inputs changed: " + JSON.stringify(change));
		});	
	});
```

The callback will be passed an object with API keys that changed mapped to the new values. After this event, the CoolClimateCalculatorHost#inputs and CoolClimateCalculatorHost#results will also have been updated.